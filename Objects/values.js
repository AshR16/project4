function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

   let objectValues=[]
   for(let i in obj){
    objectValues.push(obj[i])
   }

   return objectValues
}

module.exports=values