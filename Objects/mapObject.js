
function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    
    let objects=[]
    for(let i in obj){
        objects[i]=cb(obj[i])
    }

return objects
}
module.exports=mapObject