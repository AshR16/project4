const defaults = require("../defaults.js")

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

let result= defaults(testObject, {name: 'Sonu Nigam', age: 40, location: 'India'})

// when testOject is undefined

//let result= defaults({}, {name: 'Sonu Nigam', age: 40, location: 'India'})

console.log(result)