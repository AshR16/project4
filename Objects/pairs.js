function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let objects=[]
    for(let i in obj){
        objects.push([i,obj[i]])
    }
    return objects
}

module.exports= pairs
